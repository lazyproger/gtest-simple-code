// Небольшие примеры кода

// Инициализация vue

const app = new Vue({
    el: '#app',
    store,
    components: {
        App,
        'componentfooter': ComponentFooter,
    },
    router,
    created() {
        global(this);
    }
});

function global(app) {
    //Передаем app в $func
    app.$func.app = app;
    //Авторизуемся
    app.$func.login();
    //Слушаем localStorage
    app.$func.listenLocalStorage();
    //Слушаем клики по ссылкам
    app.$func.listenLink(router);
    //Слушаем нажатия клавиш
    app.$func.listenKeyboard();
    //Обновляем кеш
    app.$func.updateHash();

    /**
     * файл Functions на класс решил пока не переделывать
     */
}


/**********
 * Разное *
 **********/

/**
 Данный кусок кода позволяет слушать событие onpaste
 Если в буфере обмена содержиться файл, результатом выполнения кода
 будет сохранненный во vuex экземпляр файла, который мы с помощью formData
 сможем отправить на сервер
 */

document.onpaste = function (event) {
    let items = (event.clipboardData || event.originalEvent.clipboardData).items;

    for (let index in items) {

        if (items[index].kind === 'file') {
            let blob = items[index].getAsFile();

            app.$store.commit('transitionFile', blob); //app == vue
        }
    }
}

/**
 * LocalStorage
 */

/**
 С помощью запущенного слушателя listenLocalStorage
 Мы можем передавать в соседние вкладки события
 В данном примере при выходе пользователя, logout происходит во всех вкладках
 */

let Functions = {
    /**
     * ...
     */

    listenLocalStorage() {
        window.addEventListener("storage",
            (e) => {
                switch (e.key) {
                    case 'login' :
                        this.localStorageLogin(e.newValue);
                        break;
                }
            }
        );
    },
    localStorageLogin(value) {
        if (value === "true") {
            this.login(() => {
                location.reload()
            });
        } else {
            this.app.$user.logout();
        }
    },

    /**
     * ...
     */
}

/**
 listenKeyboard
 */

let Functions = {
    /**
     * ...
     */

    listenKeyboard() {
        window.onkeyup = (e) => {
            if (typeof this['keyboardEvent' + e.code] === 'function') {
                this['keyboardEvent' + e.code]();
            }
        };
    },
    keyboardEventEscape() {
        /**
         * zIndexElementsHide - закрывает все модальные окна и прочие всплывающие элементы
         */
        this.app.$store.commit('zIndexElementsHide');
    },

    /**
     * ...
     */
}