"use strict";

const user = {

    install(Vue, options = {}) {

        const data = new Vue({
            data: function () {
                return {
                    guest: true,
                    check: false,
                    data: {
                        id: 0,
                    }
                }
            },
            methods: {
                //countMessage
                countMessage(count){
                    options.store.commit('userCountMessage', count)
                },
                //Быстрые записи
                logout(){
                    this.mutation_logout();
                },
                //Взаимодействие с store
                mutation_setUser(state){
                    this.guest = false;
                    this.check = true;
                    this.data = state.user;
                },
                //Выход пользователя
                mutation_logout(){
                    if(!this.guest){
                        localStorage['login'] = false;
                        this.guest = true;
                        this.check = false;
                        this.data  = false;
                        options.store.commit('logout');
                    }
                }
            }

        });


        /**
         * Подписываемся на все вызванные мутации
         */
        options.store.subscribe((mutation, state) => {
            if(typeof data["mutation_"+mutation.type] == "function"){
                data["mutation_"+mutation.type](state);
            }
        });

        Object.defineProperty(Vue.prototype, '$user', {
            get () {
                return data;
            },
        })
    }
};

export default user;
